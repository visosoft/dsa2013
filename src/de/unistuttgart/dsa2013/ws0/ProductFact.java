/**
 * 
 */
package de.unistuttgart.dsa2013.ws0;

/**
 * @author Maximilian Visotschnig, Frank Merkle, Alessandro Tridico
 * @history 2013-04-16 MV 1.0 Erste Version
 * @version 2013-04-16 MV 1.0
 * 
 */
public enum ProductFact {
	NAME, DESCRIPTION, MANUFACTURER_NAME, MANUFACTURER_STREET, MANUFACTURER_CITY, MANUFACTURER_COUNTRY
}
